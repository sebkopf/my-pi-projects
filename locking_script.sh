#!/bin/sh
# This script always only runs one instance at a time.
# Additional instances called while still running are aborted
# rather than queued up (-n parameter of the flock call). 
# For additional info, check http://jdimpson.livejournal.com/5685.html
ME=`basename $0`;
LOCK="/tmp/${ME}.lock";
exec 9>$LOCK; 

if /usr/bin/flock -n -x 9; then :
echo "$(date): Starting $ME (PID=$$) with an exclusive lock.";
else
echo "$(date): Failed to start $ME because it is already running (only one instance allowed).";
exit 1; # adjust error code as needed
fi



sleep 150; # do something more useful here
