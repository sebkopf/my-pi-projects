#!/bin/bash

# script that uploades all files in the FILES directory to dropbox
# requires the dropbox_uploader to be configured (run it once to setup)
# inspired by: http://www.codeproject.com/Articles/768623/Raspberry-Pi-HD-surveillance-camera-with-motion-an
# This script always only runs one instance at a time.

# to run as a cronjob, add the following to crontab -e
#MAILTO=VALID.EMAIL@DOMAIN.COM
#*/15 * * * * /bin/bash /home/pi/code/motion_record/upload_pictures.sh >>/tmp/upload_pictures.log 2>&1


ME=`basename $0`;
LOCK="/tmp/${ME}.lock";
exec 9>$LOCK; 

if /usr/bin/flock -n -x 9; then :
echo "$(date): Starting $ME (PID=$$) with an exclusive lock.";
else
echo "$(date): Failed to start $ME because it is already running (only one instance allowed).";
exit 1; # adjust error code as needed
fi

FILES=/home/pi/pictures/*.jpg
UPLOADER=/home/pi/code/motion_record/dropbox_uploader.sh

echo "Looking for pictures..."
N_UP=0;
N_ERR=0;
for f in $FILES
do
    if [ -f $f ];
    then
      echo "Processing picture '$f' ..."
      $UPLOADER upload $f /
      ECODE=$?
      if [ $ECODE -eq 0 ];
      then
        N_UP=$((N_UP + 1))
        echo "Upload successful, deleting local copy."
	mv $f /home/pi/pictures/backup/
        rm -f $f
      else
         N_ERR=$((N_ERR + 1))
         echo "ERROR: could not upload '$f'. Exit code $ECODE."
      fi
    fi
done

echo "$N_UP uploaded successfully. $N_ERR could not be uploaded."
#if [ $N_UP -ne 0 ];
#then
#echo "Emailing notificaton..."
#don't email now
#echo "$N_UP new pictures were uploaded to the dropbox ($N_ERR could not be uploaded)." | mail -s "Pictures uploaded to dropbox" seb.kopf@gmail.com
fi
##echo "All done."
#echo "Script Test" | mail -s "SUCCESS" seb.kopf@gmail.com

echo "" # new line
